<?php
/**
 * Admin page callbacks for bookkeeping.
 */

/**
 * Form constructor for the bookkeeping settings form.
 *
 * @see bookkeeping_settings_form_submit()
 */
function bookkeeping_settings_form($form, &$form_state) {
  $form['accounts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Accounts'),
    '#description' => t('Select accounts used automatically by rules.'),
    '#tree' => TRUE,
  );
  $accounts = variable_get('bookkeeping_configurable_accounts', array());
  foreach (bookkeeping_configurable_accounts() as $machine_name => $account) {
    // Skip it if it has it's own UI.
    if (!$account['show on form']) {
      continue;
    }

    // Add our form element.
    $form['accounts'][$machine_name] = array(
      '#type' => 'select',
      '#title' => $account['label'],
      '#description' => $account['description'],
      '#default_value' => isset($accounts[$machine_name]) ? $accounts[$machine_name] : NULL,
      '#options' => bookkeeping_account_options($account['type']),
      '#empty_value' => '',
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submission handler for bookkeeping_settings_form().
 */
function bookkeeping_settings_form_submit(&$form, &$form_state) {
  $accounts = variable_get('bookkeeping_configurable_accounts', array());
  $accounts = array_merge($accounts, $form_state['values']['accounts']);
  variable_set('bookkeeping_configurable_accounts', $accounts);
}

/**
 * Page callback for bookkeeping rules overview.
 */
function bookkeeping_rules_overview() {
  drupal_set_title(t('Bookkeeping rules'));

  RulesPluginUI::$basePath = 'admin/config/bookkeeping/rules';
  $options = array('show plugin' => FALSE);

  $content['enabled']['title']['#markup'] = '<h3>' . t('Enabled transaction rules') . '</h3>';

  $conditions = array(
    'plugin' => 'reaction rule',
    'tags' => array('bookkeeping'),
    'active' => TRUE,
  );
  $content['enabled']['rules'] = RulesPluginUI::overviewTable($conditions, $options);
  $content['enabled']['rules']['#empty'] = t('There are no active transaction rules.');

  $content['disabled']['title']['#markup'] = '<h3>' . t('Disabled transaction rules') . '</h3>';

  $conditions['active'] = FALSE;
  $content['disabled']['rules'] = RulesPluginUI::overviewTable($conditions, $options);
  $content['disabled']['rules']['#empty'] = t('There are no disabled transaction rules.');

  return $content;
}
