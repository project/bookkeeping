<?php
/**
 * @file
 * Default pages for Bookkeeping.
 */

/**
 * Implementation of hook_default_page_manager_pages().
 */
function bookkeeping_default_page_manager_pages() {
  $export = array();

  // Scan directory for any .page files
  $files = file_scan_directory(dirname(__FILE__) . '/page_manager', '/\.page$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$page->name] = $page;
    }
  }

  return $export;
}
