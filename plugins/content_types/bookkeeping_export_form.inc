<?php
/**
 * @file
 * CTools content for transaction export form.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Transaction export form'),
  'content_types' => 'bookkeeping_export_form',
  // 'single' means not to be subtyped.
  'single' => TRUE,
  // Name of a function which will render the block.
  'render callback' => 'bookkeeping_export_form_render',

  // Icon goes in the directory with the content type.
  'description' => t('Render the transaction export form.'),
  'edit form' => 'bookkeeping_export_form_edit_form',

  // presents a block which is used in the preview of the data.
  // Pn Panels this is the preview pane shown on the panels building page.
  'category' => array(t('Bookkeeping'), 0),
);

/**
 * Render callback.
 */
function bookkeeping_export_form_render($subtype, $conf, $args, $context) {
  // Set up the block and return.
  $block = new stdClass();
  $block->title = t('Generate transaction export');
  $block->content = drupal_get_form('bookkeeping_export_form_form');

  return $block;
}

/**
 * Form constructor for the transaction export form.
 *
 * @see bookkeeping_export_form_form_validate()
 * @see bookkeeping_export_form_form_submit()
 */
function bookkeeping_export_form_form($form, &$form_state) {
  // Make sure this file gets included.
  form_load_include($form_state, 'inc', 'bookkeeping', 'plugins/content_types/bookkeeping_export_form');

  $form['intro'] = array(
    '#prefix' => '<p>',
    '#markup' => t('To export transactions you must first generate a batch. Batching allows you to capture when transactions were exported and prevent transactions from being modified once exported.'),
    '#suffix' => '</p>',
  );

  // Get hold of the earliest un-exported transaction.
  $earliest_transaction = db_query('SELECT MIN(created) FROM {bookkeeping_transaction} WHERE batch IS NULL')->fetchField();
  if (!$earliest_transaction) {
    $form['no_transactions'] = array(
      '#prefix' => '<div class="messages warning">',
      '#markup' => t('There are no un-exported transactions.'),
      '#suffix' => '</div>',
    );
    return $form;
  }

  // Put all the advanced setting in a collapsible fieldset.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Allow customization of the batch suffix.
  $batch_prefix = date('ymd', REQUEST_TIME);
  $form['advanced']['suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch name'),
    '#description' => t('To specify the batch suffix, enter it here. It must be 2 characters and not be purely numeric. It will be prefixed with %prefix and full batch name must not already be used. If not given, an incremental number will be automatically assigned.', array(
      '%prefix' => $batch_prefix,
    )),
    '#field_prefix' => $batch_prefix . '-',
    '#maxlength' => 2,
    '#size' => 5,
    '#attributes' => array(
      'style' => 'text-transform:uppercase;',
    ),
  );

  // Allow you to specify the start time.
  $form['advanced']['start'] = array(
    '#type' => 'date',
    '#title' => t('Start date'),
    '#description' => t('Specify the first day to export. This will default to the earliest un-exported transaction.'),
    '#default_value' => array(
      'year' => date('Y', $earliest_transaction),
      'month' => date('n', $earliest_transaction),
      'day' => date('j', $earliest_transaction),
    ),
  );

  // Allow you to specify the end time.
  $default_end = max($earliest_transaction, strtotime('yesterday', REQUEST_TIME));
  $form['advanced']['end'] = array(
    '#type' => 'date',
    '#title' => t('End date'),
    '#description' => t('Specify the last day to export.'),
    '#default_value' => array(
      'year' => date('Y', $default_end),
      'month' => date('n', $default_end),
      'day' => date('j', $default_end),
    ),
  );

  // Set up the submit buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate batch'),
  );

  return $form;
}

/**
 * Validation handler for bookkeeping_export_form_form().
 *
 * @see bookkeeping_export_form_form_submit()
 */
function bookkeeping_export_form_form_validate(&$form, &$form_state) {
  if (!empty($form_state['values']['suffix'])) {
    // Convert to uppercase.
    $form_state['values']['suffix'] = strtoupper($form_state['values']['suffix']);

    // Check it is 2 characters long.
    if (!strlen($form_state['values']['suffix']) != 2) {
      form_set_error('suffix', t('Batch suffix must be exactly 2 characters long.'));
    }
    // Check it is alphanumeric.
    if (!ctype_alnum($form_state['values']['suffix'])) {
      form_set_error('suffix', t('Batch suffix must not be made up of letters and numbers.'));
    }
    // Check the suffix isn't numeric.
    elseif (is_numeric($form_state['values']['suffix'])) {
      form_set_error('suffix', t('Batch suffix must not be numeric to prevent conflicts with automatically generated batch codes.'));
    }
    // Otherwise let's check it doesn't already exist.
    else {
      $controller = entity_get_controller('bookkeeping_transaction');
      $batch = date('ymd', REQUEST_TIME) . '-' . $form_state['values']['suffix'];
      if (!$controller->checkBatchName($batch)) {
        form_set_error('suffix', t('Batch name %batch already exists.', array('%batch' => $batch)));
      }
    }
  }
}

/**
 * Submission handler for bookkeeping_export_form_form().
 *
 * @see bookkeeping_export_form_form_validate()
 */
function bookkeeping_export_form_form_submit(&$form, &$form_state) {
  // Get our batch name.
  $batch = NULL;
  if (!empty($form_state['values']['suffix'])) {
    $batch = date('ymd', REQUEST_TIME) . '-' . $form_state['values']['suffix'];
  }

  // Get our end time.
  $end = mktime(23, 59, 59, $form_state['values']['end']['month'], $form_state['values']['end']['day'], $form_state['values']['end']['year']);

  // Get our start time.
  $start = mktime(0, 0, 0, $form_state['values']['start']['month'], $form_state['values']['start']['day'], $form_state['values']['start']['year']);

  // Run our assign.
  $assigned = 0;
  $controller = entity_get_controller('bookkeeping_transaction');
  $batch_id = $controller->generateBatch($batch, $end, $start, $assigned);

  if ($assigned) {
    drupal_set_message(format_plural($assigned,
      'One transaction assigned to batch %batch.',
      '@count transactions assigned to batch %batch.',
      array('%batch' => $batch_id)));
  }
  else {
    drupal_set_message(t('There were no transactions to assign.'), 'warning');
  }
}

/**
 * Config Form
 */
function bookkeeping_export_form_edit_form($form, &$form_state) {
  return $form;
}

function bookkeeping_export_form_edit_form_submit(&$form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
