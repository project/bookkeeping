<?php
/**
 * @file
 * Definition of bookkeeping_handler_field_date_advanced.
 */

/**
 * A handler to extend the date field handler for better aggregation.
 */
class bookkeeping_handler_field_date_created extends views_handler_field_date {

  function option_definition() {
    $options = parent::option_definition();

    $options['db_format'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['db_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Database format'),
      '#description' => t('If not empty, this formatting will be applied at the database, allowing you to aggregate etc.'),
      '#default_value' => isset($this->options['db_format']) ? $this->options['db_format']: '',
    );
  }

  function query() {
    if (!empty($this->options['db_format'])) {
      $this->ensure_my_table();

      $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();

      // Adjust the field.
      $field = $this->table_alias . '.' . $this->real_field;
      $field = "DATE_FORMAT(FROM_UNIXTIME({$field}), '{$this->options['db_format']}')";

      $alias = $this->table_alias . '_' . $this->real_field;

      // Add the field.
      $this->field_alias = $this->query->add_field(NULL, $field, $alias, $params);

      $this->add_additional_fields();
    }
    else {
      parent::query();
    }
  }

  function get_value($values, $field = NULL) {
    $value = parent::get_value($values, $field);

    // If we're dealing with this field, convert it back to a timestamp.
    if ($field === NULL && !empty($value) && !empty($this->options['db_format'])) {
      $value = strtotime($value);
    }

    return $value;
  }

}
