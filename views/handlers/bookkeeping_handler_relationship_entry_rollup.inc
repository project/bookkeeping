<?php
/**
 * @file
 * Views relationship handler to join an entry without rollup.
 */

/**
 * Views relationship handler to join an entry without rollup.
 *
 * This will only apply if the entry is in an account with rollup disabled.
 */
class bookkeeping_handler_relationship_entry_rollup extends views_handler_relationship {

  function option_definition() {
    $options = parent::option_definition();
    $options['account_relationship'] = array('default' => NULL);
    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['account_relationship'] = array(
      '#type' => 'select',
      '#title' => t('Account relationship'),
      '#description' => t('Select the relationship containing the account for the entry in the above relationship.'),
      '#weight' => $form['relationship']['#weight'] + 0.01,
      '#options' => array(),
    );

    foreach ($this->view->display_handler->get_option('relationships') as $relationship) {
      // relationships can't link back to self. But also, due to ordering,
      // relationships can only link to prior relationships.
      if ($this->id == $relationship['id']) {
        break;
      }
      $relationship_handler = views_get_handler($relationship['table'], $relationship['field'], 'relationship');
      // ignore invalid/broken relationships.
      if (empty($relationship_handler)) {
        continue;
      }

      // If this relationship is valid for this type, add it to the list.
      $data = views_fetch_data($relationship['table']);
      $base = $data[$relationship['field']]['relationship']['base'];
      $base_fields = views_fetch_fields($base, $form_state['type'], $this->view->display_handler->use_group_by());
      if (isset($base_fields['taxonomy_term_data.tid_representative'])) {
        $relationship_handler->init($this->view, $relationship);
        $form['account_relationship']['#options'][$relationship['id']] = $relationship_handler->label();
      }
    }
  }

  /**
   * Add the additional conditions to our join.
   */
  function query() {
    // Add a join to the rollup table.
    $relationship = $this->view->relationship[$this->options['account_relationship']];
    $table_alias = $this->query->ensure_table('field_data_bookkeeping_rollup', $relationship->alias);

    // Add our extra conditions.
    $this->definition['extra'][] = array(
      'table' => $table_alias,
      'field' => 'bookkeeping_rollup_value',
      'value' => '0',
    );

    // And pass on for the usual.
    parent::query();
  }

  /**
   * We need the account rollup relationship to be set for this to work.
   */
  function broken() {
    return empty($this->options['account_relationship']);
  }
}
