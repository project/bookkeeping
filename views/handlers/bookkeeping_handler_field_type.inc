<?php
/**
 * @file
 * Definition of bookkeeping_handler_field_date_advanced.
 */

/**
 * A handler to display debit/credit transaction types.
 */
class bookkeeping_handler_field_type extends views_handler_field {

  /**
   * Overrides views_handler_field::option_definition().
   *
   * Set the defaults for additional options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['display_values'][BOOKKEEPING_DEBIT] = t('Debit');
    $options['display_values'][BOOKKEEPING_CREDIT] = t('Credit');

    return $options;
  }

  /**
   * Overrides views_handler_field::options_form().
   *
   * Allow choices for how debit and credit get rendered.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['display_values'] = array(
      '#type' => 'fieldset',
      '#title' => t('Display values'),
      '#description' => t('Choose the rendered display for the different types.'),
      '#tree' => TRUE,
    );

    $form['display_values'][BOOKKEEPING_DEBIT] = array(
      '#type' => 'textfield',
      '#title' => t('Debit'),
      '#default_value' => $this->options['display_values'][BOOKKEEPING_DEBIT],
    );

    $form['display_values'][BOOKKEEPING_CREDIT] = array(
      '#type' => 'textfield',
      '#title' => t('Credit'),
      '#default_value' => $this->options['display_values'][BOOKKEEPING_CREDIT],
    );
  }

  /**
   * Overrides views_handler_field::render().
   *
   * Convert the type integers into their labels.
   */
  function render($values) {
    $value = $this->get_value($values);
    if ($value !== NULL && isset($this->options['display_values'][$value])) {
      return $this->options['display_values'][$value];
    }
  }

}
