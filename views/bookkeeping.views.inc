<?php
/**
 * @file
 * Views related functions.
 */

/**
 * Implements hook_views_data_alter().
 */
function bookkeeping_views_data_alter(&$data) {
  // Set to use special handlers for fields.
  $data['bookkeeping_transaction']['created']['field']['handler'] = 'bookkeeping_handler_field_date_created';
  $data['bookkeeping_transaction']['created']['filter']['handler'] = 'views_handler_filter_date';
  $data['bookkeeping_transaction']['created']['filter']['is_date'] = TRUE;

  $data['bookkeeping_entry']['type']['field']['handler'] = 'bookkeeping_handler_field_type';

  // Add a join using our rollup field.
  $data['bookkeeping_entry']['bookkeeping_entry_rollup'] = array(
    'title' => t('Transaction entry (obeying rollup)'),
    'relationship' => array(
      'base' => 'bookkeeping_entry',
      'field' => 'entry_id',
      'handler' => 'bookkeeping_handler_relationship_entry_rollup',
      'label' => t('Rollup entry'),
      'help' => t('Join to a bookkeeping entry based on whether the account the entry belongs to should be rolled up.'),
    ),
  );
}
