<?php
/**
 * @file
 * bookkeeping.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bookkeeping_views_default_views() {
  $views = array();

  // Scan this directory for any .view files
  $files = file_scan_directory(dirname(__FILE__) . '/defaults', '/\.view$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}

/**
 * Implements hook_views_default_views_alter().
 */
function bookkeeping_views_default_views_alter(&$views) {
  // Scan this directory for any .view_alter files
  $files = file_scan_directory(dirname(__FILE__) . '/defaults', '/\.view_alter$/', array('key' => 'name'));
  foreach ($files as $file) {
    if (isset($views[$file->name])) {
      // Set the views variable and include the file.
      $view =& $views[$file->name];
      include $file->uri;
    }
  }
}
