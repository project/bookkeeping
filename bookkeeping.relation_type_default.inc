<?php
/**
 * @file
 * Exported relation types for Bookkeeping.
 */

/**
 * Implements hook_relation_default_relation_types().
 */
function bookkeeping_relation_default_relation_types() {
  $relation_types = array();

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE; /* Edit this to true to make a default relation_type disabled initially */
  $relation_type->api_version = 1;
  $relation_type->relation_type = 'bookkeeping_transaction_rel';
  $relation_type->label = 'relates to';
  $relation_type->reverse_label = 'relates to';
  $relation_type->directional = 1;
  $relation_type->transitive = 0;
  $relation_type->r_unique = 0;
  $relation_type->min_arity = 2;
  $relation_type->max_arity = 0;
  $relation_type->source_bundles = array(
    'bookkeeping_transaction:*',
  );
  $relation_type->target_bundles = array(
    'commerce_booking_ticket:*',
    'commerce_order:*',
    'commerce_payment_transaction:*',
    'node:event',
    'party:*',
    'rules_config:*',
  );
  $relation_types['bookkeeping_transaction_rel'] = $relation_type;

  return $relation_types;
}
