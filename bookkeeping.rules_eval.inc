<?php
/**
 * @file
 * Callbacks for rules.
 */

/**
 * Action: Post transaction.
 *
 * Posts a simple transaction from one account to the other.
 *
 * @param array $value
 *   A commerce price field item for the value of the transaction. If negative,
 *   the source and destination will be swapped and a positive value will be
 *   posted. If zero value, we do nothing.
 * @param stdClass $source
 *   A taxonomy term from the bookkeeping_accounts vocabulary - the account we
 *   are to credit.
 * @param stdClass $destination
 *   A taxonomy term from the bookkeeping_accounts vocabulary - the account we
 *   are to credit.
 * @param string $description
 *   A description for the transaction.
 * @param array $settings
 *   Optionally an action's configuration settings.
 *
 * @return array|FALSE
 *   Return FALSE on failure or otherwise an array with of the structure:
 *   - transaction: The newly created bookkeeping transaction entity.
 */
function bookkeeping_rules_post_transaction($value, $source, $destination, $description, $settings = array()) {
  // A zero value transaction or no source or destination is meaningless.
  if ($value['amount'] == 0 || !$source || !$destination) {
    return FALSE;
  }
  // If we have a negative value transaction, switch the source and destination
  // around and absolute the amount.
  elseif ($value['amount'] < 0) {
    $value['amount'] = abs($value['amount']);

    $source_orig = $source;
    $source = $destination;
    $destination = $source_orig;
    unset($source_orig);
  }

  // Create our transaction and get the wrapper.
  $transaction = entity_create('bookkeeping_transaction', array(
    'description' => $description,
    'created' => REQUEST_TIME,
  ));

  // Add our entries.
  $entry = bookkeeping_entry_create(array(
    'description' => $description,
    'type' => BOOKKEEPING_CREDIT,
    'bookkeeping_account' => $source,
    'bookkeeping_amount' => $value,
  ), $transaction);

  $entry = bookkeeping_entry_create(array(
    'description' => $description,
    'type' => BOOKKEEPING_DEBIT,
    'bookkeeping_account' => $destination,
    'bookkeeping_amount' => $value,
  ), $transaction);

  // Save the transaction and return.
  $transaction->save();
  return array('transaction' => $transaction);
}

/**
 * Action: Retrieve account.
 *
 * Retrieve a configurable account.
 *
 * @param string $machine_name
 *   The machine name of the configurable account.
 *
 * @return object|FALSE
 *   Return FALSE on failure or a account taxonomy term on success.
 */
function bookkeeping_rules_retrieve_account($machine_name) {
  // Retrieve the list of configurable accounts.
  $accounts = variable_get('bookkeeping_configurable_accounts', array());

  // If set, load and return the term.
  if (isset($accounts[$machine_name])) {
    return array('account' => taxonomy_term_load($accounts[$machine_name]));
  }

  // Otherwise return FALSE.
  return FALSE;
}
