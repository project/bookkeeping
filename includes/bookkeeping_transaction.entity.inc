<?php
/**
 * @file
 * Entity and controller for bookkeeping transactions.
 */

/**
 * Class for bookkeeping transaction entities.
 */
class BookkeepingTransaction extends Entity {

  /**
   * The transaction id.
   *
   * @var integer
   */
  public $transaction_id;

  /**
   * The description of the transaction.
   *
   * @var string
   */
  public $description;

  /**
   * The Unix timestamp when the transaction was created.
   *
   * @var integer
   */
  public $created;

  /**
   * The batch identifier for the export.
   *
   * @var string
   */
  public $batch;

  /**
   * Overrides Entity::__construct().
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'bookkeeping_transaction');
  }

  /**
   * Overrides Entity::defaultUri().
   */
  protected function defaultUri() {
    return array('path' => 'admin/transactions/' . $this->identifier());
  }

}

/**
 * Controller for bookkeeping transactions.
 */
class BookkeepingTransactionController extends EntityAPIController {

  /**
   * Gets the next batch suffix.
   *
   * @param $date
   *   The date we want to get the suffix for in the format YYMMDD.
   *
   * @return string
   *   The generated batch name.
   */
  public function getBatchName($date = NULL) {
    // If there is no date, use todays.
    if (!$date) {
      $date = date('ymd', REQUEST_TIME);
    }

    // Look up the highest numeric value for this date.
    $query = db_select('bookkeeping_transaction', 'bt');
    $query->fields('bt', array('batch'));
    $query->distinct();
    $query->condition('batch', "$date%", 'LIKE');
    $query->orderBy('batch', 'DESC');
    $results = $query->execute()->fetchCol();

    // Find the first numeric value.
    $suffix = 0;
    foreach ($results as $row) {
      $row = substr($row, 7);
      if (is_numeric($row)) {
        $suffix = (int) $row;
        break;
      }
    }

    // Increment our batch.
    $suffix++;

    return $date . '-' . str_pad($suffix, 2, 0, STR_PAD_LEFT);
  }

  /**
   * Checks a batch name doesn't already exist.
   *
   * @param string $batch
   *   The batch name that we want to check.
   *
   * @return boolean
   *   TRUE if the batch doesn't exist, FALSE if it does.
   */
  function checkBatchName($batch) {
    return (bool) db_query('SELECT COUNT(*) FROM {bookkeeping_transaction} WHERE batch = :batch', array(
        ':batch' => $batch,
      ))->fetchField();
  }

  /**
   * Generate a batch for the given entities.
   *
   * @param string $batch
   *   Optionally a batch of the format ymd-??. If not given, it will be
   *   automatically incremented from the most recent for today.
   * @param integer $end
   *   A UNIX timestamp for the latest timed transaction to include in this
   *   batch. If empty, it will take the request time. If NULL there will be no
   *   end time.
   * @param integer $start
   *   A UNIX timestamp for the earliest timed transaction to include in this
   *   batch. If NULL there will be no start time.
   * @param integer $assigned
   *   Optionally provide an argument by reference that will be assigned with the
   *   number of entries assigned to the batch.
   *
   * @return string
   *   The full batch identifier assigned.
   */
  public function generateBatch($batch = NULL, $end = REQUEST_TIME, $start = NULL, &$assigned = NULL) {
    // Get our batch code.
    if (!$batch) {
      $batch = $this->getBatchName();
    }

    // Build our update query.
    $query = db_update('bookkeeping_transaction');
    $query->fields(array(
      'batch' => $batch,
    ));
    $query->condition('batch', NULL);
    if (isset($end)) {
      $query->condition('created', $end, '<=');
    }
    if (isset($start)) {
      $query->condition('created', $start, '>=');
    }

    $assigned = $query->execute();

    return $batch;
  }

}
