<?php
/**
 * @file
 * Bookkeeping related hooks.
 */

/**
 * Implements hook_bookkeeping_account_type_info().
 */
function bookkeeping_bookkeeping_account_type_info() {
  return array(
    'asset' => array(
      'label' => 'Asset',
      'positive' => BOOKKEEPING_DEBIT,
    ),
    'liability' => array(
      'label' => 'Liability',
      'positive' => BOOKKEEPING_CREDIT,
    ),
    'income' => array(
      'label' => 'Income',
      'positive' => BOOKKEEPING_CREDIT,
    ),
    'expense' => array(
      'label' => 'Expense',
      'positive' => BOOKKEEPING_DEBIT,
    ),
    'capital' => array(
      'label' => 'Capital',
      'positive' => BOOKKEEPING_CREDIT,
    ),
  );
}

/**
 * Implements hook_bookkeeping_account_overview_form_alter()
 */
function bookkeeping_bookkeeping_account_overview_form_alter(&$form, &$form_state) {
  // Remove the reset to alphabetical button as it's not relevant.
  $form['actions']['reset_alphabetical']['#access'] = FALSE;
}

/**
 * Implements hook_bookkeeping_account_overview_table_alter()
 */
function bookkeeping_bookkeeping_account_overview_table_alter(&$variables) {
  // Output the account type as well.
  $position = 2;
  array_splice($variables['header'], $position, 0, array(t('Type'), t('Code'), t('Department')));
  foreach ($variables['rows'] as $key => &$row) {
    $key = explode(':', $key);
    $term = taxonomy_term_load($key[1]);
    $display = array('label' => 'hidden');
    $output = array();
    $output[] = render(field_view_field('taxonomy_term', $term, 'bookkeeping_account_type', $display));
    $output[] = render(field_view_field('taxonomy_term', $term, 'bookingkeeping_account_code', $display));
    $output[] = render(field_view_field('taxonomy_term', $term, 'bookingkeeping_account_department', $display));
    array_splice($row['data'], $position, 0, $output);
  }
}

/**
 * Implements hook_bookkeeping_configurable_accounts().
 */
function bookkeeping_bookkeeping_configurable_accounts() {
  return array(
    'accounts_receivable' => array(
      'label' => t('Accounts receivable'),
      'description' => t('A general accounts receivable account.'),
      'type' => 'asset',
    ),
  );
}
