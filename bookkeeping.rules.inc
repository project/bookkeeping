<?php
/**
 * @file
 * Contains rules hooks and helper functions for bookkeeping.
 */

/**
 * Implements hook_rules_file_info().
 */
function bookkeeping_rules_file_info() {
  return array('bookkeeping.rules_eval');
}

/**
 * Implements hook_rules_action_info().
 */
function bookkeeping_rules_action_info() {
  return array(
    'bookkeeping_retrieve_account' => array(
      'label' => t('Get a configured account'),
      'group' => t('Bookkeeping'),
      'parameter' => array(
        'machine_name' => array(
          'type' => 'text',
          'label' => t('Account'),
          'options list' => 'bookkeeping_rules_configured_account_options',
          'description' => t('Select the configurable account you want.'),
          'restriction' => 'input',
        ),
      ),
      'provides' => array(
        'account' => array(
          'type' => 'taxonomy_term',
          'label' => t('Retrieved account'),
          'save' => FALSE,
        ),
      ),
      'base' => 'bookkeeping_rules_retrieve_account',
      'access callback' => 'bookkeeping_rules_access',
    ),
    'bookkeeping_post_transaction' => array(
      'label' => t('Post a transaction'),
      'group' => t('Bookkeeping'),
      'parameter' => array(
        'value' => array(
          'label' => t('Value'),
          'type' => 'commerce_price',
          'description' => t('The value of the transaction to post.'),
        ),
        'source' => array(
          'label' => t('Source account'),
          'type' => 'taxonomy_term',
          'description' => t('The account to credit.'),
          'save' => FALSE,
        ),
        'destination' => array(
          'label' => t('Destination account'),
          'type' => 'taxonomy_term',
          'description' => t('The account to debit.'),
          'save' => FALSE,
        ),
        'description' => array(
          'label' => t('Description'),
          'type' => 'text',
          'description' => t('The description for this transaction.'),
          'optional' => TRUE,
          'default value' => '',
          'allow null' => TRUE,
        ),
      ),
      'provides' => array(
        'transaction' => array(
          'label' => t('Transaction'),
          'type' => 'bookkeeping_transaction',
          'save' => TRUE,
        ),
      ),
      'base' => 'bookkeeping_rules_post_transaction',
      'access callback' => 'bookkeeping_rules_access',
    ),
  );
}

/**
 * Bookkeeping rules access callback.
 */
function bookkeeping_rules_access($type, $name) {
  return user_access('administer bookkeeping');
}

/**
 * Options callback for configurable account getter.
 */
function bookkeeping_rules_configured_account_options() {
  // Get the accounts and reduce to an options list.
  $accounts = bookkeeping_configurable_accounts();
  foreach ($accounts as &$account) {
    $account = $account['label'];
  }
  return $accounts;
}
