<?php
/**
 * @file
 * Hooks provided by Commerce Booking.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define account types for bookkeeping.
 *
 * This allows modules to define account types for bookkeeping.
 *
 * @return array
 *   An array of account types keyed by name. Each account type is an
 *   associative with the following key-value pairs:
 *   - "label": Required. The untranslated human friendly label.
 *   - "positive": Required. The type of transaction that increases the value
 *     in the account. This should be either BOOKKEEPING_CREDIT or
 *     BOOKKEEPING_DEBIT.
 */
function hook_bookkeeping_account_type_info() {
  return array(
    'asset' => array(
      'label' => 'Asset',
      'positive' => BOOKKEEPING_DEBIT,
    ),
  );
}

/**
 * Modify the account types for bookkeeping.
 *
 * @param array $types
 *   An array of types as described by the return of
 *   hook_bookkeeping_account_type_info().
 */
function hook_bookkeeping_account_type_info_alter(&$types) {
  $types['income']['label'] = 'Revenue';
}

/**
 * Modify the account overview form before it is rendered.
 *
 * @param $form
 *   A Form API form array.
 * @param $form_state
 *   A Form API form_state array.
 */
function hook_bookkeeping_account_overview_form_alter(&$form, &$form_state) {
  // Remove the reset to alphabetical button as it's not relevant.
  $form['action']['reset_alphabetical']['#access'] = FALSE;
}

/**
 * Modify the account overview table before it is rendered.
 *
 * @param $variables
 *   An associative array as given to theme_table().
 */
function hook_bookkeeping_account_overview_table_alter(&$varaibles) {
  // No example.
}

/**
 * Declare UI configurable accounts.
 *
 * Configurable accounts allow end user selection of accounts for exported
 * rules provided by modules.
 *
 * @return array
 *   Outer keys are the configurable account machine name and each item is an
 *   array of:
 *   - label: The label for the configurable account.
 *   - description: Optionally a description for use on the form.
 *   - type: Optionally a type of account that is valid.
 *   - show on form: Boolean indicating whether to show on the default admin
 *     form. If FALSE, the module will have to provide it's own UI.
 *
 * @see bookkeeping_configurable_accounts()
 * @see hook_bookkeeping_configurable_accounts_alter()
 */
function hook_bookkeeping_configurable_accounts() {
  return array(
    'accounts_receivable' => array(
      'label' => t('Accounts receivable'),
      'description' => t('A general accounts receivable account.'),
      'type' => array('asset'),
    ),
  );
}

/**
 * Alter UI configurable accounts.
 *
 * @param $accounts array
 *   The accounts to be altered.
 *
 * @see bookkeeping_configurable_accounts()
 * @see hook_bookkeeping_configurable_accounts()
 */
function hook_bookkeeping_configurable_accounts_alter(&$accounts) {
  $accounts['accounts_receivable']['label'] = t('General accounts receivable');
}

/**
 * @} End of "addtogroup hooks".
 */
