<?php
/**
 * @file
 * Contains rules hooks and helper functions for bookkeeping.
 */

/**
 * Implements hook_rules_file_info().
 */
function bookkeeping_commerce_rules_file_info() {
  return array('bookkeeping_commerce.rules_eval');
}

/**
 * Implements hook_rules_action_info().
 */
function bookkeeping_commerce_rules_action_info() {
  return array(
    'bookkeeping_commerce_get_payment_account' => array(
      'label' => t('Get payment account'),
      'group' => t('Bookkeeping Commmerce'),
      'parameter' => array(
        'payment_method' => array(
          'label' => t('Payment method'),
          'type' => 'text',
          'description' => t('The payment method we want to retrieve an account for.'),
        ),
      ),
      'provides' => array(
        'account' => array(
          'label' => t('Account'),
          'type' => 'taxonomy_term',
          'save' => FALSE,
        ),
      ),
      'base' => 'bookkeeping_commerce_rules_get_payment_account',
      'access callback' => 'bookkeeping_rules_access',
    ),
  );
}
