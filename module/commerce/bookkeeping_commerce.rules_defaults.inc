<?php
/**
 * @file
 * Default rules and helpers for building them.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function bookkeeping_commerce_default_rules_configuration() {
  // Load the include for the helpers.
  module_load_include('inc', 'bookkeeping', 'bookkeeping.rules_defaults');
  $configs = array();

  /**
   * Payment rules.
   */
  if (module_exists('commerce_payment')) {
    // Payment created.
    $rule = bookkeeping_rules_transaction_base('Payment created', 'commerce_payment_transaction_insert', 'Commerce Payment', 'commerce_payment');
    bookkeeping_commerce_rules_transaction_condition_payment_status($rule['IF'], FALSE);
    bookkeeping_commerce_rules_transaction_condition_payment_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_payment_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-rec', 'account-pay');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-payment-transaction',
      'commerce-payment-transaction:order',
    ), 'bookkeeping_commerce_payment_created');
    $configs['bookkeeping_commerce_payment_created'] = rules_import(array('bookkeeping_commerce_payment_created' => $rule));

    // Payment deleted.
    $rule = bookkeeping_rules_transaction_base('Payment deleted', 'commerce_payment_transaction_delete', 'Commerce Payment', 'commerce_payment');
    bookkeeping_commerce_rules_transaction_condition_payment_status($rule['IF'], FALSE);
    bookkeeping_commerce_rules_transaction_condition_payment_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_payment_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-pay', 'account-rec');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-payment-transaction',
      'commerce-payment-transaction:order',
    ), 'bookkeeping_commerce_payment_deleted');
    $configs['bookkeeping_commerce_payment_deleted'] = rules_import(array('bookkeeping_commerce_payment_deleted' => $rule));

    // Payment status to paid.
    $rule = bookkeeping_rules_transaction_base('Payment status to paid', 'commerce_payment_transaction_update', 'Commerce Payment', 'commerce_payment');
    bookkeeping_commerce_rules_transaction_condition_payment_status($rule['IF']);
    bookkeeping_commerce_rules_transaction_condition_payment_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_payment_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-rec', 'account-pay');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-payment-transaction',
      'commerce-payment-transaction:order',
    ), 'bookkeeping_commerce_payment_to_paid');
    $configs['bookkeeping_commerce_payment_to_paid'] = rules_import(array('bookkeeping_commerce_payment_to_paid' => $rule));

    // Payment status from paid.
    $rule = bookkeeping_rules_transaction_base('Payment status from paid', 'commerce_payment_transaction_update', 'Commerce Payment', 'commerce_payment');
    bookkeeping_commerce_rules_transaction_condition_payment_status($rule['IF'], TRUE, FALSE);
    bookkeeping_commerce_rules_transaction_condition_payment_value($rule['IF'], TRUE);
    bookkeeping_commerce_rules_transaction_action_payment_value($rule['DO'], FALSE, TRUE);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-pay', 'account-rec');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-payment-transaction',
      'commerce-payment-transaction:order',
    ), 'bookkeeping_commerce_payment_from_paid');
    $configs['bookkeeping_commerce_payment_from_paid'] = rules_import(array('bookkeeping_commerce_payment_from_paid' => $rule));

    // Payment value changed.
    $rule = bookkeeping_rules_transaction_base('Payment value changed', 'commerce_payment_transaction_update', 'Commerce Payment', 'commerce_payment');
    bookkeeping_commerce_rules_transaction_condition_payment_status($rule['IF'], TRUE, NULL);
    bookkeeping_commerce_rules_transaction_condition_payment_value($rule['IF'], NULL);
    bookkeeping_commerce_rules_transaction_action_payment_value($rule['DO'], TRUE);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-rec', 'account-pay');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-payment-transaction',
      'commerce-payment-transaction:order',
    ), 'bookkeeping_commerce_payment_value_change');
    $configs['bookkeeping_commerce_payment_value_change'] = rules_import(array('bookkeeping_commerce_payment_value_change' => $rule));
  }

  /**
   * Order rules
   */
  if (module_exists('commerce_order')) {
    // Commerce order created.
    $rule = bookkeeping_rules_transaction_base('Order created', 'commerce_order_insert', 'Commerce Order', 'commerce_order');
    bookkeeping_commerce_rules_transaction_conditions_order_base($rule['IF'], 'commerce_order');
    bookkeeping_commerce_rules_transaction_condition_order_status($rule['IF'], FALSE);
    bookkeeping_commerce_rules_transaction_conditions_order_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_order_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-inc', 'account-rec');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-order',
    ), 'bookkeeping_commerce_order_created');
    $configs['bookkeeping_commerce_order_created'] = rules_import(array('bookkeeping_commerce_order_created' => $rule));

    // Commerce order deleted.
    $rule = bookkeeping_rules_transaction_base('Order deleted', 'commerce_order_delete', 'Commerce Order', 'commerce_order');
    bookkeeping_commerce_rules_transaction_conditions_order_base($rule['IF'], 'commerce_order');
    bookkeeping_commerce_rules_transaction_condition_order_status($rule['IF'], FALSE, TRUE);
    bookkeeping_commerce_rules_transaction_conditions_order_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_order_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-rec', 'account-inc');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-order',
    ), 'bookkeeping_commerce_order_deleted');
    $configs['bookkeeping_commerce_order_deleted'] = rules_import(array('bookkeeping_commerce_order_deleted' => $rule));

    // Commerce order to owed.
    $rule = bookkeeping_rules_transaction_base('Order to owed', 'commerce_order_update', 'Commerce Order', 'commerce_order');
    bookkeeping_commerce_rules_transaction_conditions_order_base($rule['IF'], 'commerce_order');
    bookkeeping_commerce_rules_transaction_condition_order_status($rule['IF']);
    bookkeeping_commerce_rules_transaction_conditions_order_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_order_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-inc', 'account-rec');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-order',
    ), 'bookkeeping_commerce_order_to_owed');
    $configs['bookkeeping_commerce_order_to_owed'] = rules_import(array('bookkeeping_commerce_order_to_owed' => $rule));

    // Commerce order from owed.
    $rule = bookkeeping_rules_transaction_base('Order to owed', 'commerce_order_update', 'Commerce Order', 'commerce_order');
    bookkeeping_commerce_rules_transaction_conditions_order_base($rule['IF'], 'commerce_order');
    bookkeeping_commerce_rules_transaction_condition_order_status($rule['IF'], TRUE, TRUE);
    bookkeeping_commerce_rules_transaction_conditions_order_value($rule['IF']);
    bookkeeping_commerce_rules_transaction_action_order_value($rule['DO']);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-rec', 'account-inc');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-order',
    ), 'bookkeeping_commerce_order_from_owed');
    $configs['bookkeeping_commerce_order_from_owed'] = rules_import(array('bookkeeping_commerce_order_from_owed' => $rule));

    // Commerce order value changed.
    $rule = bookkeeping_rules_transaction_base('Order value changed', 'commerce_order_update', 'Commerce Order', 'commerce_order');
    bookkeeping_commerce_rules_transaction_conditions_order_base($rule['IF'], 'commerce_order');
    bookkeeping_commerce_rules_transaction_condition_order_status($rule['IF'], NULL);
    bookkeeping_commerce_rules_transaction_conditions_order_value($rule['IF'], TRUE);
    bookkeeping_commerce_rules_transaction_action_order_value($rule['DO'], TRUE);
    bookkeeping_rules_transaction_action_post_transaction($rule['DO'], 'txn_value', 'account-inc', 'account-rec');
    bookkeeping_rules_transaction_action_transaction_relation($rule['DO'], array(
      'commerce-order',
    ), 'bookkeeping_commerce_order_value_changed');
    $configs['bookkeeping_commerce_order_value_changed'] = rules_import(array('bookkeeping_commerce_order_value_changed' => $rule));

  }
  return $configs;
}

/**
 * Helper function for conditions of payment status.
 *
 * @param array $conditions
 *   The 'IF' key of a rule import array.
 * @param boolean $original
 *   Whether the rule has an original to compare with.
 * @param boolean $active
 *   Whether the payment should be changing to paid. If NULL it will check the
 *   payment both was and is paid.
 */
function bookkeeping_commerce_rules_transaction_condition_payment_status(array &$conditions, $original = TRUE, $active = TRUE) {
  // Current status of payment.
  $conditions[][$active !== FALSE ? 'data_is': 'NOT data_is'] = array(
    'data' => array('commerce-payment-transaction:status'),
    'value' => 'success',
  );

  // If we had an original, check the status has changed.
  if ($original) {
    $conditions[][$active === TRUE ? 'NOT data_is': 'data_is'] = array(
      'data' => array('commerce-payment-transaction-unchanged:status'),
      'value' => 'success',
    );
  }
}

/**
 * Helper function for conditions of payment having a value to post.
 *
 * @param array $conditions
 *   The 'IF' key of a rule import array.
 * @param boolean|NULL $original
 *   Whether we want to run the value check on the original. If NULL, compare
 *   the value of the old and original.
 */
function bookkeeping_commerce_rules_transaction_condition_payment_value(array &$conditions, $original = FALSE) {
  // Check the ticket has/had a value.
  if (isset($original)) {
    $conditions[]['NOT data_is'] = array(
      'data' => array('commerce-payment-transaction' . ($original ? '-unchanged': '') . ':amount'),
      'op' => '=',
      'value' => 0,
    );
  }
  // Otherwise compare the current and original values.
  else {
    $conditions[]['NOT data_is'] = array(
      'data' => array('commerce-payment-transaction:amount'),
      'value' => array('commerce-payment-transaction-unchanged:amount'),
    );
  }
}

/**
 * Helper function for action of getting the payment value.
 *
 * @param array $actions
 *   The 'DO' key of a rule import array.
 * @param boolean $change
 *   Whether we want to calculate the change in value.
 * @param boolean $original
 *   Whether we want to take the value from the un-changed entity.
 */
function bookkeeping_commerce_rules_transaction_action_payment_value(array &$actions, $change = FALSE, $original = FALSE) {
  // Set up our value variable.
  $actions[]['variable_add'] = array(
    'USING' => array(
      'type' => 'commerce_price',
      'value' => array(
        'value' => array(
          'amount' => 0,
          'currency_code' => 'GBP',
        ),
      ),
    ),
    'PROVIDE' => array(
      'variable_added' => array(
        'txn_value' => 'Value',
      ),
    ),
  );

  // Set our currency code.
  $actions[]['data_set'] = array(
    'data' => array('txn_value:currency_code'),
    'value' => array('commerce-payment-transaction:currency_code'),
  );

  if ($change) {
    // Perform our price calculation.
    $actions[]['data_calc'] = array(
      'USING' => array(
        'input_1' => array('commerce-payment-transaction:amount'),
        'op' => '-',
        'input_2' => array('commerce-payment-transaction-unchanged:amount'),
      ),
      'PROVIDE' => array(
        'result' => array(
          'value_amount' => 'Value amount',
        ),
      ),
    );

    // Set the new amount on the value variable.
    $actions[]['data_set'] = array(
      'data' => array('txn_value:amount'),
      'value' => array('value-amount'),
    );
  }
  else {
    // Get the value from the transaction.
    $actions[]['data_set'] = array(
      'data' => array('txn_value:amount'),
      'value' => array('commerce-payment-transaction' . ($original ? '-unchanged': '') . ':amount'),
    );
  }

  // Get the payment account.
  $actions[]['bookkeeping_commerce_get_payment_account'] = array(
    'USING' => array(
      'payment_method' => array('commerce-payment-transaction:payment-method'),
    ),
    'PROVIDE' => array(
      'account' => array(
        'account_pay' => 'Account',
      ),
    ),
  );

  // Get the accounts receivable account.
  bookkeeping_rules_transaction_action_retrieve_account($actions, 'accounts_receivable', 'account_rec', 'Accounts receivable');
}

/**
 * Helper function for base conditions on an order.
 *
 * @param array $conditions
 *   The 'IF' key of a rule import array.
 * @param string $order_bundle
 *   The bundle of the commerce_order.
 * @param boolean $exclude
 *   Whether the rule should exclude the specified bundle.
 * @param boolean $active
 *   Whether the order should be changing to active. If NULL it will check the
 *   payment both was and is paid.
 */
function bookkeeping_commerce_rules_transaction_conditions_order_base(array &$conditions, $order_bundle, $exclude = FALSE) {
  // Put a condition on the ticket type.
  $conditions[][$exclude ? 'NOT data_is': 'data_is'] = array(
    'data' => array('commerce-order:type'),
    'value' => $order_bundle,
  );
}

/**
 * Helper function for conditions of order status.
 *
 * @param array $conditions
 *   The 'IF' key of a rule import array.
 * @param boolean $original
 *   Whether the rule has an original to compare with. If NULL, it will check
 *   that both current and original match the given statuses.
 * @param boolean $exclude
 *   Whether the check should exclude the given statuses.
 * @param string|array $statuses
 *   A single or an array of commerce order statuses.
 */
function bookkeeping_commerce_rules_transaction_condition_order_status(array &$conditions, $original = TRUE, $exclude = FALSE, $statuses = array('pending', 'processing', 'completed')) {
  $statuses = (array) $statuses;
  $statuses = drupal_map_assoc($statuses);

  // Current status of payment.
  $conditions[][$exclude ? 'NOT data_is': 'data_is'] = array(
    'data' => array('commerce-order:status'),
    'op' => 'IN',
    'value' => array(
      'value' => $statuses,
    ),
  );

  // If we had an original, check the status has changed.
  if ($original !== FALSE) {
    $conditions[][$exclude && isset($original) ? 'data_is': 'NOT data_is'] = array(
      'data' => array('commerce-order-unchanged:status'),
      'op' => 'IN',
      'value' => array(
        'value' => $statuses,
      ),
    );
  }
}

/**
 * Helper function for conditions of orders having a value to post.
 *
 * @param array $conditions
 *   The 'IF' key of a rule import array.
 * @param boolean|NULL $original
 *   Whether we want to run the value check on the original. If NULL, compare
 *   the value of the old and original.
 */
function bookkeeping_commerce_rules_transaction_conditions_order_value(array &$conditions, $original = FALSE) {
  // Check the line item has/had a value.
  if (isset($original)) {
    $conditions[]['data_is'] = array(
      'data' => array('commerce-order' . ($original ? '-unchanged': '') . ':commerce-order-total:amount'),
      'op' => '>',
      'value' => 0,
    );
  }
  // Otherwise compare the current and original values.
  else {
    $conditions[]['NOT data_is'] = array(
      'data' => array('commerce-order:commerce-order-total:amount'),
      'value' => array('commerce-order-unchanged:commerce-order-total:amount'),
    );
  }
}

/**
 * Helper function for action of getting the order value.
 *
 * @param array $actions
 *   The 'DO' key of a rule import array.
 * @param boolean $change
 *   Whether we want to calculate the change in value.
 * @param boolean $original
 *   Whether we want to take the value from the un-changed entity.
 */
function bookkeeping_commerce_rules_transaction_action_order_value(array &$actions, $change = FALSE, $original = FALSE) {
  // Set up our value variable.
  $actions[]['variable_add'] = array(
    'USING' => array(
      'type' => 'commerce_price',
      'value' => array(
        'value' => array(
          'amount' => 0,
          'currency_code' => 'GBP',
        ),
      ),
    ),
    'PROVIDE' => array(
      'variable_added' => array(
        'txn_value' => 'Value',
      ),
    ),
  );

  // Set our currency code.
  $actions[]['data_set'] = array(
    'data' => array('txn_value:currency_code'),
    'value' => array('commerce-order:commerce-order-total:currency_code'),
  );

  if ($change) {
    // Perform our price calculation.
    $actions[]['data_calc'] = array(
      'USING' => array(
        'input_1' => array('commerce-order:commerce-order-total:amount'),
        'op' => '-',
        'input_2' => array('commerce-order-unchanged:commerce-order-total:amount'),
      ),
      'PROVIDE' => array(
        'result' => array(
          'value_amount' => 'Value amount',
        ),
      ),
    );

    // Set the new amount on the value variable.
    $actions[]['data_set'] = array(
      'data' => array('txn_value:amount'),
      'value' => array('value-amount'),
    );
  }
  else {
    // Get the value from the transaction.
    $actions[]['data_set'] = array(
      'data' => array('txn_value:amount'),
      'value' => array('commerce-order' . ($original ? '-unchanged': '') . ':commerce-order-total:amount'),
    );
  }

  // Get the accounts receivable account.
  bookkeeping_rules_transaction_action_retrieve_account($actions, 'accounts_receivable', 'account_rec', 'Accounts receivable');

  // Get the income account.
  bookkeeping_rules_transaction_action_retrieve_account($actions, 'commerce_order', 'account_inc', 'Income account');
}
