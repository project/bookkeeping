<?php
/**
 * @file
 * Bookkeeping related hooks.
 */

/**
 * Implements hook_bookkeeping_configurable_accounts().
 */
function bookkeeping_commerce_bookkeeping_configurable_accounts() {
  $accounts = array();

  if (module_exists('commerce_payment')) {
    foreach (commerce_payment_methods() as $method_id => $payment_method) {
      $accounts['commerce_payment_' . $method_id] = array(
        'label' => $payment_method['title'],
        'show on form' => FALSE,
      );
    }
  }

  if (module_exists('commerce_order')) {
    $accounts['commerce_order'] = array(
      'label' => t('Commerce order'),
      'show on form' => FALSE,
    );
  }

  return $accounts;
}
