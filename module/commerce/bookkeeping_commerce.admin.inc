<?php
/**
 * @file
 * Contain administrative parts of bookkeeping commerce.
 */

/**
 * Form constructor for bookkeeping commerce config form.
 *
 * @see bookkeeping_commerce_config_form_submit().
 */
function bookkeeping_commerce_config_form($form, &$form_state) {
  $accounts = variable_get('bookkeeping_configurable_accounts', array());

  if (module_exists('commerce_payment')) {
    $form['payment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Commerce payment'),
      '#description' => t('These settings control how changes to payments create transactions.'),
      '#tree' => TRUE,
    );

    foreach (commerce_payment_methods() as $method_id => $payment_method) {
      $form['payment']['accounts']['commerce_payment_' . $method_id] = array(
        '#type' => 'select',
        '#title' => $payment_method['title'],
        '#description' => t('Choose an asset account for %title to pay into.', array('%title' => $payment_method['title'])),
        '#options' => bookkeeping_account_options(),
        '#default_value' => isset($accounts['commerce_payment_' . $method_id]) ? $accounts['commerce_payment_' . $method_id] : NULL,
        '#empty_value' => '',
      );
    }
  }
  else {
    $form['payment'] = array(
      '#type' => 'item',
      '#weight' => 99,
      '#title' => t('Commerce payment'),
      '#markup' => t('Enable commerce payment to allow bookkeeping to track payment transactinos.'),
    );
  }

  if (module_exists('commerce_order')) {
    $form['order'] = array(
      '#type' => 'fieldset',
      '#title' => t('Commerce order'),
      '#description' => t('These settings control how changes to orders create transactions.'),
      '#tree' => TRUE,
    );

    $form['order']['accounts']['commerce_order'] = array(
      '#type' => 'select',
      '#title' => t('Order income account'),
      '#description' => t('Choose an income account to pay into.'),
      '#options' => bookkeeping_account_options(),
      '#default_value' => isset($accounts['commerce_order']) ? $accounts['commerce_order'] : NULL,
      '#empty_value' => '',
    );
  }
  else {
    $form['order'] = array(
      '#type' => 'item',
      '#weight' => 99,
      '#title' => t('Commerce order'),
      '#markup' => t('Enable commerce order to allow bookkeeping to track order changes.'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
* Form submission handler for bookkeeping_commerce_config_form().
*/
function bookkeeping_commerce_config_form_submit($form, &$form_state) {
  $accounts = variable_get('bookkeeping_configurable_accounts', array());

  if (module_exists('commerce_payment')) {
    $accounts = array_merge($accounts, $form_state['values']['payment']['accounts']);
  }

  if (module_exists('commerce_order')) {
    $accounts = array_merge($accounts, $form_state['values']['order']['accounts']);
  }

  variable_set('bookkeeping_configurable_accounts', $accounts);
}
