<?php
/**
 * @file
 * Callbacks for rules.
 */

/**
 * Action: Get payment account.
 *
 * Return a payment account for a payment method.
 *
 * @param string $payment_method
 *   The commerce payment method.
 * @param array $settings
 *   Optionally an action's configuration settings.
 *
 * @return stdClass|NULL
 *   An account taxonomy term or NULL if not available.
 */
function bookkeeping_commerce_rules_get_payment_account($payment_method, $settings = array()) {
    $accounts = variable_get('bookkeeping_configurable_accounts', array());
  if (isset($accounts['commerce_payment_' . $payment_method])) {
    $return['account'] = taxonomy_term_load($accounts['commerce_payment_' . $payment_method]);
    return $return;
  }
}
