<?php
/**
 * @file
 * Helpers for building rules in code.
 */

/**
 * Helper function for constructing a bookkeeping transaction rule.
 *
 * @param string $label
 *   The label for the rule.
 * @param string|array $on
 *   An single event or an array of events to run on.
 * @param string|array $tags
 *   An single or array of additional tags to apply to the rule.
 * @param string|array $requires
 *   An single or array of additional required modules to apply to the rule.
 *
 * @return array
 *   An import array for rules_import().
 */
function bookkeeping_rules_transaction_base($label, $on, $tags = array(), $requires = array()) {
  $rule = array(
    'LABEL' => $label,
    'PLUGIN' => 'reaction rule',
    'TAGS' => array_merge(array('Bookkeeping'), (array) $tags),
    'REQUIRES' => array_merge(array('rules', 'bookkeeping'), (array) $requires),
    'ON' => (array) $on,
    'IF' => array(),
    'DO' => array(),
  );

  return $rule;
}

/**
 * Helper function for retreiving a configurable account.
 *
 * @param array $actions
 *   The 'DO' key of a rule import array.
 * @param string $account
 *   The machine name of the configurable account.
 * @param string $variable
 *   The name of the variable to provide.
 * @param string $label
 *   The label of the variable to provide.
 */
function bookkeeping_rules_transaction_action_retrieve_account(array &$actions, $account = 'accounts_receivable', $variable = 'account', $label = 'Account') {
  $actions[]['bookkeeping_retrieve_account'] = array(
    'USING' => array(
      'machine_name' => $account,
    ),
    'PROVIDE' => array(
      'account' => array(
        $variable => $label,
      ),
    ),
  );
}

/**
 * Helper function for action of posting a transaction.
 *
 * @param array $actions
 *   The 'DO' key of a rule import array.
 * @param string $value
 *   The selector for the value of the transaction.
 * @param integer|string $source
 *   The ID of the source account or a selector for the account.
 * @param integer $destination
 *   The ID of the destination account or a selector for the account.
 * @param string $description
 *   Optionally the description for the transaction.
 */
function bookkeeping_rules_transaction_action_post_transaction(array &$actions, $value, $source, $destination, $description = '') {
  $actions[]['bookkeeping_post_transaction'] = array(
    'USING' => array(
      'value' => array($value),
      'source' => is_string($source) ? array($source): $source,
      'destination' => is_string($destination) ? array($destination): $destination,
      'description' => $description,
    ),
    'PROVIDE' => array(
      'transaction' => array(
        'transaction' => 'Transaction',
      ),
    ),
  );
}

/**
 * Helper function for action of relating to entities.
 *
 * @param array $actions
 *   The 'DO' key of a rule import array.
 * @param array $entities
 *   An array of selectors for the entities to relate to.
 * @param string $rules_config
 *   Optionally pass the identifier of this rules config to relate to.
 */
function bookkeeping_rules_transaction_action_transaction_relation(array &$actions, $entities, $rules_config = NULL) {
  // Build our variable.
  $actions[]['variable_add'] = array(
    'USING' => array(
      'type' => 'list<entity>',
      'value' => NULL,
    ),
    'PROVIDE' => array(
      'variable_added' => array(
        'related_entities' => 'Related entities',
      ),
    ),
  );

  // Add the transaction.
  $actions[]['list_add'] = array(
    'list' => array('related-entities'),
    'item' => array('transaction'),
  );

  // Add the additional entities.
  foreach ($entities as $entity) {
    $actions[]['list_add'] = array(
      'list' => array('related-entities'),
      'item' => array($entity),
    );
  }

  // Add the rules config if provided.
  if ($rules_config) {
    $actions[]['variable_add'] = array(
      'USING' => array(
        'type' => 'rules_config',
        'value' => $rules_config,
      ),
      'PROVIDE' => array(
        'variable_added' => array(
          'rules_config' => 'Rule',
        ),
      ),
    );

    $actions[]['list_add'] = array(
      'list' => array('related-entities'),
      'item' => array('rules-config'),
    );
  }

  // Create the relationship.
  $actions[]['entity_create'] = array(
    'USING' => array(
      'type' => 'relation',
      'param_relation_type' => 'bookkeeping_transaction_rel',
      'param_endpoints' => array('related-entities'),
    ),
    'PROVIDE' => array(
      'entity_created' => array(
        'relation' => 'Relation',
      ),
    ),
  );

  // Save the entity.
  $actions[]['entity_save'] = array(
    'data' => array('relation'),
  );
}
